"""Provide DRF serializers for request and response data of REST endpoints.

This module provides serializers for planner API endpoints and meeting backend
endpoints. These serializers serve two purposes:

1. enable the automatic generation of OpenAPI documentation for an endpoint;
2. implement validation of the payload to an endpoint.

"""
from rest_framework import serializers
from rest_framework.schemas.openapi import AutoSchema


class AppointmentSerializer(serializers.Serializer):
    """Enable serialization of the payload to endpoint appointments."""

    appointment_type = serializers.IntegerField(
        help_text="Id of the type of appointment"
    )
    date = serializers.DateField(help_text="Date of appointment as YYYY-MM-DD (UTC)")
    start_time = serializers.TimeField(
        help_text="Start time of appointment as HH:MM (UTC)"
    )
    source_user_id = serializers.CharField(
        max_length=255,
        required=False,
        help_text="Optional source (external) system UUID of user that schedules appointment",
    )
    first_name = serializers.CharField(
        max_length=64,
        help_text="First name of customer that schedules appointment",
    )
    last_name = serializers.CharField(
        max_length=64, help_text="Last name of customer that schedules appointment"
    )
    email = serializers.EmailField(
        help_text="Email address of customer to send confirmation email to"
    )
    phone = serializers.CharField(
        max_length=64,
        required=False,
        allow_blank=True,
        default="",
        help_text="Phone number of customer",
    )
    mobile_phone = serializers.CharField(
        max_length=64,
        required=False,
        allow_blank=True,
        default="",
        help_text="Mobile phone number of customer",
    )
    description = serializers.CharField(
        max_length=256,
        required=False,
        allow_blank=True,
        default="",
        help_text="Additional information about the meeting",
    )
    subject = serializers.CharField(
        max_length=128,
        required=False,
        allow_blank=True,
        default="",
        help_text="Subject of the meeting",
    )
    reserve_room = serializers.BooleanField(
        default=False, help_text="Reserve room for videocall"
    )


class StatusSerializer(serializers.Serializer):
    """Enable serialization of key 'status' of planner API responses.

    Each endpoint of the planner API returns a dict with a key that specifies
    whether the request was successful. The purpose of this serializer is to
    let the OpenAPI specification document the format of that part of the
    response.

    """

    status = serializers.BooleanField(
        help_text="True unless the response by the meeting backend indicates an error"
    )
    message = serializers.CharField(
        help_text="Description of the meeting backend error, if one occurred"
    )


class SetAppointmentResponse(serializers.Serializer):
    """Enable serialization of the response data of a meeting backend endpoint.

    planner API endpoint appointments returns response data received from
    meeting backend endpoint setAppointment. This serializer for the response
    data of setAppointment allows the DRF to automatically document the format
    of the planner API response.

    """

    Id = serializers.IntegerField(help_text="Id of new or updated element")
    Status = serializers.IntegerField(help_text="Status of new or updated element")


class AppointmentResponseSerializer(serializers.Serializer):
    """Enable serialization of the response data by endpoint appointments."""

    status = StatusSerializer()
    items = SetAppointmentResponse(
        many=True,
        help_text="Response elements of the meeting backend; empty if the meeting backend response status indicates an error",
    )


class AppointmentSchema(AutoSchema):
    """Set the serializers for the request and response data of endpoint appointments.

    The format of the response data of meeting backend setAppointment differs a
    lot from the format of the request data. Because of that difference
    endpoint appointments requires two serializers.

    """

    def get_request_serializer(self, path, method):
        """Override the standard serializer for the request data."""
        return AppointmentSerializer()

    def get_response_serializer(self, path, method):
        """Override the standard serializer for the response data."""
        return AppointmentResponseSerializer()
