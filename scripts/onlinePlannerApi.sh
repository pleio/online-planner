#!/usr/bin/env bash

# Utility script to debug API call without the wrapper.
# This script expects three arguments to be provided, namely:
# The online-planner API key as the first argument
# The online-planner Secret as the second argument
# The resource specific query string portion of the online-planner API as the third argument. 
# Do not include the `api_key`, 'api_signature' and 'api_salt' parameters in the queryString parameter.
# Important: The query string parameters should be provided sorted in alphabetical order by key!

# For example: 
# ./onlinePlannerApi.sh 123key146-api1236 1293912319longsecret123134mock method=getFields

key="$1"
secret="$2"
queryString="$3"
trimmedQueryString=$( echo "$queryString" | sed 's/[&=]//g')
salt=$(date +%s)
signature=$(echo -n "$trimmedQueryString$secret$salt" | openssl dgst -sha256 | awk '{print $2}')

url="https://agenda.onlineafspraken.nl/APIREST?$queryString&api_key=$key&api_signature=$signature&api_salt=$salt"

echo "Trimmed query string: $trimmedQueryString"
echo "Salt: $salt"
echo "Signature: $signature"
echo "Url: $url"

curl $url
