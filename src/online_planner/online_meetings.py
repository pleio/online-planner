import datetime
import hashlib
import logging
import time
import xml.etree.ElementTree as ElementTree
from typing import Any, Callable, Dict, List, Optional, Tuple, Union, cast

import requests

from .exceptions import (
    BackendRequestError,
    BackendResponseContentError,
    CreateNewCustomerError,
    MultipleEnabledCustomersError,
)
from .settings_container import SettingsContainerBase

logger = logging.getLogger(__name__)


# Some endpoints of the meeting backend require parameters which you have to
# retrieve from other endpoints of the meeting backend or from other external
# resources. For example, endpoint setAppointment requires an the agenda ID,
# which is available from endpoint getAgendas. The next get_.*_params functions
# retrieve a specific set of parameters from an endpoint. Each function returns
# these parameters as a dictionary that maps parameter name to value.


def get_agenda_params(request_params):
    """Return the dictionary that specifies the agenda ID."""
    xml_content = online_meetings_get("getAgendas")

    agendas = extract_items_from_xml("Agenda", xml_content)
    agenda_id = agendas[0]["Id"]

    return {"AgendaId": agenda_id}


def get_or_create_customer_params(request_params):
    """Return the dictionary that specifies the customer ID.

    This function looks for the customer with the email address that is
    specified by the given request parameters. If no such customer exists, this
    function creates it.

    """
    xml_content = online_meetings_get(
        "getCustomers", {"Email": request_params["email"]}
    )
    customer_id = get_or_create_customer_id(
        xml_content,
        request_params["first_name"],
        request_params["last_name"],
        request_params["email"],
        request_params.get("phone"),
        request_params.get("mobile_phone"),
        request_params.get("source_user_id"),
    )

    return {"CustomerId": customer_id}


def get_standard_create_appointment_params(request_params):
    """Return the dictionary of params specified by the given request params."""
    appointment_params = {
        "AppointmentTypeId": request_params["appointment_type"],
        "Date": request_params["date"],
        "StartTime": request_params["start_time"],
    }

    description = request_params["description"]
    if description:
        appointment_params["Description"] = description

    # This custom field is specifically used by the online planner
    # of the Dossier project
    belastingdienst_subject = request_params.get("subject")
    if belastingdienst_subject:
        appointment_params[
            "Ik_heb_een_vraag_of_opmerking_over"
        ] = belastingdienst_subject

    return appointment_params


def online_meetings_get(method: str, params: Dict[str, Union[str, int]] = None) -> str:
    """Send a GET request to the given method of onlineafspraken.nl.

    onlineafspraken.nl expects the request to have a certain payload that in
    effect signs the request, see the `API documentation`_ for details. This
    function constructs these elements of the payload.

    Returns:
        content of the response, in XML format

    Raises:
        requests.RequestException: can be raised by requests.get, which is used
            to send the request

        apps.planner.exceptions.BackendResponseContentError: when attribute
        <Response>/<Status>/<Status> contains another value than "success"

    .. _API documentation:
       https://onlineafspraken.nl/en_US/developers/hoe-werkt-het

    """
    return online_meetings_request(requests.get, method, params)


def online_meetings_put(method: str, params: Dict[str, Union[str, int]] = None) -> str:
    """Send a PUT request to the given method of onlineafspraken.nl.

    onlineafspraken.nl expects the request to have a certain payload that in
    effect signs the request, see the `API documentation`_ for details. This
    function constructs these elements of the payload.

    Returns:
        content of the response, in XML format

    Raises:
        requests.RequestException: can be raised by requests.get, which is used
            to send the request

        apps.planner.exceptions.BackendResponseContentError: when attribute
        <Response>/<Status>/<Status> contains another value than "success"

    .. _API documentation:
       https://onlineafspraken.nl/en_US/developers/hoe-werkt-het

    """
    return online_meetings_request(requests.put, method, params)


def online_meetings_request(
    request_method: Callable, method: str, params: Dict[str, Union[str, int]] = None
) -> str:
    timestamp = int(time.time())
    settings_container = SettingsContainerBase.build()

    params = sign_params(
        settings_container.get_secret(),
        settings_container.get_key(),
        timestamp,
        method,
        params,
    )

    log_message = f"Request to {method} of meeting backend failed"
    exception_message = "Request to meeting backend failed"

    try:
        response = request_method(settings_container.get_url(), params=params)
    except requests.RequestException as e:
        logger.exception(log_message)
        raise BackendRequestError(f"{exception_message}: {e.__class__.__name__}")

    if response.status_code >= 400:
        logger.error(f"{log_message}: {response.reason} ({response.status_code})")
        raise BackendRequestError(
            f"{exception_message}: {response.reason} ({response.status_code})"
        )

    _raise_exception_when_status_tag_indicates_failure(response.text)

    return response.text


def sign_params(
    api_secret: str,
    api_key: str,
    timestamp: int,
    method: str,
    params: Dict[str, Union[str, int]] = None,
) -> Dict[str, Union[str, int]]:
    """Sign and return the given request parameters.

    The request to an endpoint of the meeting backend has to be "signed". This
    means (among others) that you compute a hash over the params and send that
    hash along with the original parameters. [1]_. This function takes the
    original set of parameters and returns the signed set.

    Args:
       params: holds additional parameters that have to be passed to the
           endpoint

    .. [1] The link
           https://onlineafspraken.nl/nl_NL/developers/hoe-werkt-het#signing
           specifies which parameters have to be passed and how.

    """
    params = {} if params is None else params

    params_as_string = "".join((f"{key}{params[key]}" for key in sorted(params)))

    # The following does seem a bit weird but to compute the signature, we have
    # to remove any spaces from the string of parameter names and values. Note
    # that in this context newlines and carriage returns should *not be
    # considered* spaces.
    #
    # This is mentioned in the documentation of the meeting backend although
    # the exact wording leaves room for different interpretions. If we don't
    # remove the spaces, the call to the meeting backend will fail and its
    # response will state that the API signature is incorrect.
    params_as_string = params_as_string.replace(" ", "")

    unhashed_signature = f"{params_as_string}method{method}{api_secret}{timestamp}"
    h = hashlib.new("sha256")
    h.update(unhashed_signature.encode(encoding="UTF-8"))

    return params | {
        "method": method,
        "api_key": api_key,
        "api_signature": h.hexdigest(),
        "api_salt": f"{timestamp}",
    }


def extract_items_from_xml(
    element_type: str, response_content: str, sort_key: str = "Id"
) -> List[Dict[str, Union[str, int]]]:
    """Extract the elements of the given type from the given response content.

    Args:
        element_type: name of the XML tag that specifies the element type of
            interest
        response_content: content of the response, which is in XML format
        sort_key: element attribute used to sort the returned elements

    Returns:
        list of dictionaries, where each dictionary is a single item of the
             requested element type

    Each returned element is represented by the dictionary of element attribute
    name to value. The attributes are the same as those in the reponse content.
    Note that the Id of each element is returned as an int.

    """
    element = ElementTree.fromstring(response_content)

    elements_root = element.find("Objects")
    if elements_root is None:
        # onlineafspraken.nl can return a response that lacks an <Objects> tag
        # but with a single <element_type> tag without any attributes. As an
        # example, have a look at this response to a getCustomers when no
        # customers have been defined yet:
        #
        #     <?xml version="1.0" encoding="UTF-8"?>
        #     <Response>
        #       <Status>
        #         <APIVersion>1.0</APIVersion>
        #         <Date>2022-05-10 14:27:12</Date>
        #         <Timestamp>1652185632</Timestamp>
        #         <Status>success</Status>
        #         <Stats>
        #           <Limit>500</Limit>
        #           <Offset>0</Offset>
        #           <Records>0</Records>
        #           <TotalRecords>0</TotalRecords>
        #         </Stats>
        #       </Status>
        #       <Customer>
        #       </Customer>
        #     </Response>
        #
        # This seems wrong because you end up with an ill-formed <Customer>
        # element. If this happens, we return the empty list as no appropriate
        # elements are present.
        return []

    results = []
    elements = [child for child in elements_root if child.tag == element_type]
    for element in elements:
        new_result: Dict[str, Union[int, str]] = {}
        for child in element:
            text = child.text if child.text is not None else ""
            if child.tag == sort_key:
                new_result[child.tag] = int(text)
            else:
                new_result[child.tag] = text
        if new_result:
            results.append(new_result)

    return sorted(results, key=lambda elem: elem.get(sort_key, 0))


def _raise_exception_when_status_tag_indicates_failure(response_content):
    if not response_content:
        return

    element = ElementTree.fromstring(response_content)

    code = "<no code given>"
    message = "<no message given>"

    success = True
    status_element = element.find("Status")
    for child in status_element:
        if child.tag == "Status" and child.text != "success":
            success = False
        if child.tag == "Code":
            code = child.text
        if child.tag == "Message":
            message = child.text

    if not success:
        logger.error(
            "Request to endpoint of meeting backend failed: (code, message) = (%s, %s)",
            code,
            message,
        )
        raise BackendResponseContentError(message, code)


def get_or_create_customer_id(
    xml_content: str,
    first_name: str,
    last_name: str,
    email_address: str,
    phone: str = None,
    mobile_phone: str = None,
    source_user_id: str = None,
) -> int:
    """Return the ID of the single enabled customer in the given content.

    If no such customer exists, this function requests the meeting backend to
    create a new customer with given name and email address.

    Raises:
        apps.planner.exceptions.BackendError: when the request to the
            meeting backend to create a new customer failed

    """
    customer_id = extract_customer_id(xml_content)
    if customer_id is not None:
        return customer_id

    endpoint_params: Dict[str, Union[int, str]] = {
        "FirstName": first_name,
        "LastName": last_name,
        "Email": email_address,
    }

    if phone:
        endpoint_params["Phone"] = phone
    if mobile_phone:
        endpoint_params["MobilePhone"] = mobile_phone
    # Specific field for Dossier project
    if source_user_id:
        endpoint_params["Source_User_ID"] = source_user_id

    xml_content = online_meetings_put("setCustomer", endpoint_params)

    customer_id = extract_customer_id(xml_content)
    if customer_id is not None:
        return customer_id

    logger.error(
        "Request to endpoint of meeting backend failed to create new customer %s",
        email_address,
    )
    raise CreateNewCustomerError()


def extract_customer_id(xml_content: str) -> Optional[int]:
    """Return the ID of the single enabled customer in the given content.

    If no such customer exists, this function returns None.

    Raises:
        apps.planner.exceptions.MultipleCustomersError: when the content
            specifies multiple enabled customers

    """
    customers = extract_items_from_xml("Customer", xml_content)

    status_enabled = "1"
    customers = list(filter(lambda c: c["Status"] == status_enabled, customers))

    if len(customers) == 0:
        return None

    if len(customers) > 1:
        ids = ", ".join((str(c["Id"]) for c in customers))
        logger.error("The XML response content returns multiple customers: %s", ids)
        raise MultipleEnabledCustomersError()

    return cast(int, customers[0]["Id"])


def get_date_interval(date: datetime.date) -> Tuple[datetime.date, datetime.date]:
    """Return interval of dates of the bookable items.

    If you access endpoint getBookableTimes of onlineafspraken.nl to retrieve
    available dates and times, you provide the start and end date of the dates
    you're interested in. This function computes such an interval: it returns
    the interval that starts at the given date and ends 30 days later.

    """
    return date, date + datetime.timedelta(days=30)


def aggregate_bookable_items_per_date(
    items: List[Dict[str, Union[str, int]]]
) -> List[Dict[str, Any]]:
    """Aggregate the given response items per date.

    The given items is a list of bookable items. This function aggregates these
    items per date to a list such as::

        [
            {
                "Date": "2022-04-21",
                "Times": [
                    {
                        "StartTime": "09:00",
                        "EndTime": "09:30",
                    },
                    {
                        "StartTime": "09:30",
                        "EndTime": "10:00",
                    },
                ],
            }
        ]

    No other attributes of the original bookable items are preserved, this is,
    this function removes the following fields:

    `AppointmentTypeId`
        this value is already specified as an argument to the endpoint

    `ResourceId`
        this value will not be used as we leave it to the meeting backend to
        select a resource

    `Timestamp`
        there's no use for this value

    """
    # The next dict maps a date to an entry that specifies the bookable times
    # for that date, e.g.
    #
    #    { "2022-04-21":
    #      { "Date": "2022-04-21",
    #        "Times": [
    #          {
    #            "StartTime": "09:00",
    #            "LabelTime": "09:00",
    #            "EndTime": "09:30",
    #          },
    #          {
    #            "StartTime": "09:30",
    #            "LabelTime": "09:30",
    #            "EndTime": "10:00",
    #          },
    #        ],
    #      }
    #    }
    #
    # When we iterate over the bookable times, this dict allows us to quickly
    # find the entry to which each bookable time should be added.
    aggregated_items: Dict[str, Dict[str, Any]] = {}
    for item in items:
        date = cast(str, item["Date"])
        aggregated_item = aggregated_items.setdefault(date, {"Date": date, "Times": []})

        new_time = {key: item[key] for key in ("StartTime", "EndTime")}
        # The same time might have been added already due to a bookable time
        # for a different resource. Make sure you don't add it twice.
        if new_time not in aggregated_item["Times"]:
            aggregated_item["Times"].append(new_time)

    # return the list of entries sorted by date
    return [entry for _, entry in sorted(aggregated_items.items())]
