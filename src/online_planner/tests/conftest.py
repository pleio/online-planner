"""Provides pytest fixtures that are used in multiple test modules.

This name of this module, conftest.py, lets pytest find this module and its
fixtures automatically. This means that any test module can use the fixtures
defined here without explicitly importing them.

For more information about conftest.py, see section `conftest.py: sharing
fixtures across multiple files`_ of the pytest documentation.

.. _: https://docs.pytest.org/en/stable/reference/fixtures.html?highlight=conftest#conftest-py-sharing-fixtures-across-multiple-files

"""
import logging

import pytest


@pytest.fixture()
def only_log_critical(caplog):
    """Silence specific log output using a pytest fixture.

    When you decorate a unittest test method using

        @pytest.mark.usefixtures("only_log_critical")

    that test will not show the log messages of

    - django.request,
    - apps.planner.online_meetings and
    - apps.planner.views

    that have a level lower than CRITICAL. This keeps the output of the pytest
    run clean.

    """
    caplog.set_level(logging.CRITICAL, "django.request")
    caplog.set_level(logging.CRITICAL, "online_planner.online_meetings")
    caplog.set_level(logging.CRITICAL, "online_planner.video_call")
    caplog.set_level(logging.CRITICAL, "online_planner.views")
