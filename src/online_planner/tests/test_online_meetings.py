import datetime
import unittest
import unittest.mock as mock

import ddt
import pytest

from online_planner.exceptions import (
    CreateNewCustomerError,
    MultipleEnabledCustomersError,
)
from online_planner.online_meetings import (
    aggregate_bookable_items_per_date,
    extract_customer_id,
    get_date_interval,
    get_or_create_customer_id,
    get_standard_create_appointment_params,
    sign_params,
)

from .responses import CUSTOMERS_CONTENT


class TestSignParams(unittest.TestCase):
    def test_for_endpoint_without_additional_parameters(self):
        params = sign_params(
            "<api secret>", "<api key>", 1650454460, "getAppointmentTypes"
        )
        expected_params = {
            "method": "getAppointmentTypes",
            "api_key": "<api key>",
            "api_signature": "08a60720a53f21a67ac8840f13733f00d85d6119ced0a3b64e215d7b18d18fe9",
            "api_salt": "1650454460",
        }
        self.assertEqual(expected_params, params)

    def test_for_endpoint_with_additional_parameters(self):
        params = sign_params(
            "<api secret>",
            "<api key>",
            1650454460,
            "getBookableTimes",
            {"AgendaId": 12, "AppointmentTypeId": 1234, "Date": "2022-04-21"},
        )
        expected_params = {
            "method": "getBookableTimes",
            "AgendaId": 12,
            "AppointmentTypeId": 1234,
            "Date": "2022-04-21",
            "api_key": "<api key>",
            "api_signature": "9451b5ad697af7c358d0d85d81425f7942db4deb0b63268901c3d4095cda9bb8",
            "api_salt": "1650454460",
        }
        self.assertEqual(expected_params, params)

    def test_remove_spaces_to_compute_the_signature(self):
        # This test verifies the expected signature in case of a parameter
        # whose value contains spaces. If the code under test fails to remove
        # those spaces, this test will fail because the signature will be
        # different.
        #
        # Do note that if this test fails, it might be for another reason than
        # a failure to remove spaces.
        params = sign_params(
            "<api secret>",
            "<api key>",
            1650454460,
            "fakeEndpoint",
            {
                "Description": "Ik wil mijn dossier bespreken.",
            },
        )
        expected_params = {
            "method": "fakeEndpoint",
            "Description": "Ik wil mijn dossier bespreken.",
            "api_key": "<api key>",
            "api_signature": "aefbd814fae078b1ae8d8db77db6e839baf558f866bfaa78be5a221ff78ba380",
            "api_salt": "1650454460",
        }
        self.assertEqual(expected_params, params)

    def test_leave_in_returns_to_compute_the_signature(self):
        # This test verifies the expected signature in case of a parameter
        # whose value contains returns. If the code under test fails to remove
        # those returns, this test will fail because the signature will be
        # different.
        #
        # Do note that if this test fails, it might be for another reason than
        # a failure to leave in returns.
        params = sign_params(
            "<api secret>",
            "<api key>",
            1650454460,
            "fakeEndpoint",
            {
                "Description": """Ik wil mijn
 dossier bespreken.""",
            },
        )
        expected_params = {
            "method": "fakeEndpoint",
            "Description": """Ik wil mijn
 dossier bespreken.""",
            "api_key": "<api key>",
            "api_signature": "eba2ff36631f371981ced93fd6ecb21d99646490c991545f5dfc032be37e9add",
            "api_salt": "1650454460",
        }
        self.assertEqual(expected_params, params)


class TestExtractCustomerId(unittest.TestCase):
    def test_extract_correct_customer_id(self):
        self.assertEqual(35366364, extract_customer_id(CUSTOMERS_CONTENT))

    def test_return_None_when_no_customers_are_returned(self):
        xml_content = create_customer_response()
        self.assertIsNone(extract_customer_id(xml_content))

    def test_return_None_when_no_enabled_customers_are_returned(self):
        # only customers with status 1 are enabled
        xml_content = create_customer_response(
            "<Id>35366364</Id><Status>2</Status>", "<Id>35565609</Id><Status>2</Status>"
        )
        self.assertIsNone(extract_customer_id(xml_content))

    @pytest.mark.usefixtures("only_log_critical")
    def test_raise_proper_exception_when_multiple_enabled_customers_are_returned(self):
        # only customers with status 1 are enabled
        xml_content = create_customer_response(
            "<Id>35366364</Id><Status>1</Status>", "<Id>35565609</Id><Status>1</Status>"
        )
        with self.assertRaises(MultipleEnabledCustomersError):
            extract_customer_id(xml_content)


def create_customer_response(*args):
    """Return XML content that specifies zero or more customers.

    This method creates fake XML content for a call to the meeting backend
    that should yield customers. By default, it creates content that
    specifies no customers.

    If you pass a string to this method that string will be used to create a
    <Customer> tag, e.g.

    >>> create_customer_response("<Id>35366364</Id><Status>1</Status>")
    <?xml version="1.0" encoding="UTF-8"?>
    <Response>
      <Status>
        <Status>success</Status>
      </Status>
      <Objects>
        <Customer><Id>35366364</Id><Status>1</Status></Customer>
      </Objects>
    </Response>

    If you pass multiple strings, this method will create multiple customers.

    """
    if args:
        lines_to_insert = (
            ["  <Objects>"]
            + [f"   <Customer>{attrs}</Customer>" for attrs in args]
            + ["  </Objects>"]
        )
    else:
        lines_to_insert = [" <Customer></Customer>"]

    xml_to_insert = "\n".join(lines_to_insert)
    xml_content = f"""
<?xml version="1.0" encoding="UTF-8"?>
<Response>
  <Status>
    <Status>success</Status>
  </Status>
{xml_to_insert}
</Response>""".lstrip()

    return xml_content


class TestGetOrCreateCustomerId(unittest.TestCase):
    def test_extract_correct_customer_id(self):
        first_name, last_name, email_address = self.create_customer_spec()

        self.assertEqual(
            35366364,
            get_or_create_customer_id(
                CUSTOMERS_CONTENT, first_name, last_name, email_address
            ),
        )

    @staticmethod
    def create_customer_spec():
        """Return tuple of first name, last name & email address for test customer."""
        return "Hello", "World", "hello.world@example.com"

    @mock.patch("requests.put")
    def test_return_ID_of_new_customer_when_no_customers_are_returned(
        self, requests_put
    ):
        response = mock.Mock()
        response.status_code = 200
        response.text = create_customer_response("<Id>35571924</Id><Status>1</Status>")
        requests_put.return_value = response

        xml_content = create_customer_response()
        first_name, last_name, email_address = self.create_customer_spec()

        self.assertEqual(
            35571924,
            get_or_create_customer_id(
                xml_content, first_name, last_name, email_address
            ),
        )

    @pytest.mark.usefixtures("only_log_critical")
    @mock.patch("requests.put")
    def test_raise_correct_exception_when_meeting_backend_fails_to_create_new_customer(
        self, requests_put
    ):
        response = mock.Mock()
        response.status_code = 200
        response.text = create_customer_response()
        requests_put.return_value = response

        xml_content = create_customer_response()
        first_name, last_name, email_address = self.create_customer_spec()

        with self.assertRaises(CreateNewCustomerError):
            get_or_create_customer_id(xml_content, first_name, last_name, email_address)


class TestGetStandardCreateAppointmentParams(unittest.TestCase):
    def test_blank_description_is_not_passed_as_a_parameter(self):
        request_params = {
            "appointment_type": 412410,
            "date": "2022-07-11",
            "start_time": "17:00",
            "description": "",
        }
        expected_set_appointment_params = {
            "AppointmentTypeId": 412410,
            "Date": "2022-07-11",
            "StartTime": "17:00",
        }
        self.assertEqual(
            expected_set_appointment_params,
            get_standard_create_appointment_params(request_params),
        )

    def test_nonblank_description_is_passed_as_a_parameter(self):
        request_params = {
            "appointment_type": 412410,
            "date": "2022-07-11",
            "start_time": "17:00",
            "description": "Ik wil mijn dossier bespreken.",
        }
        expected_set_appointment_params = {
            "AppointmentTypeId": 412410,
            "Date": "2022-07-11",
            "StartTime": "17:00",
            "Description": "Ik wil mijn dossier bespreken.",
        }
        self.assertEqual(
            expected_set_appointment_params,
            get_standard_create_appointment_params(request_params),
        )


@ddt.ddt
class TestGetDateInterval(unittest.TestCase):
    @ddt.data(
        [datetime.date(2022, 2, 1), datetime.date(2022, 3, 3)],
        [datetime.date(2022, 12, 1), datetime.date(2022, 12, 31)],
        [datetime.date(2022, 12, 15), datetime.date(2023, 1, 14)],
    )
    @ddt.unpack
    def test_return_interval_that_ends_30_days_later(self, date, expected_last_date):
        first_date, last_date = get_date_interval(date)
        self.assertEqual(date, first_date)
        self.assertEqual(expected_last_date, last_date)


class TestAggregateBookableItemsPerDate(unittest.TestCase):
    def test_multiple_days_with_multiple_time_slots(self):
        items = [
            {
                "Date": "2022-04-21",
                "StartTime": "09:00",
                "EndTime": "09:30",
                "Timestamp": 1650524400,
                "AppointmentTypeId": 42,
                "ResourceId": 144,
            },
            {
                "Date": "2022-04-21",
                "StartTime": "09:30",
                "EndTime": "10:00",
                "Timestamp": 1650526200,
                "AppointmentTypeId": 42,
                "ResourceId": 144,
            },
            {
                "Date": "2022-04-22",
                "StartTime": "10:00",
                "EndTime": "10:30",
                "Timestamp": 1650614400,
                "AppointmentTypeId": 42,
                "ResourceId": 144,
            },
            {
                "Date": "2022-04-22",
                "StartTime": "10:30",
                "EndTime": "11:00",
                "Timestamp": 1650616200,
                "AppointmentTypeId": 42,
                "ResourceId": 144,
            },
        ]
        expected_aggregations = [
            {
                "Date": "2022-04-21",
                "Times": [
                    {
                        "StartTime": "09:00",
                        "EndTime": "09:30",
                    },
                    {
                        "StartTime": "09:30",
                        "EndTime": "10:00",
                    },
                ],
            },
            {
                "Date": "2022-04-22",
                "Times": [
                    {
                        "StartTime": "10:00",
                        "EndTime": "10:30",
                    },
                    {
                        "StartTime": "10:30",
                        "EndTime": "11:00",
                    },
                ],
            },
        ]
        self.assertEqual(
            expected_aggregations, aggregate_bookable_items_per_date(items)
        )

    def test_single_time_slot_for_multiple_resources(self):
        items = [
            {
                "Date": "2022-04-21",
                "StartTime": "09:00",
                "EndTime": "09:30",
                "Timestamp": 1650524400,
                "AppointmentTypeId": 42,
                "ResourceId": 144,
            },
            {
                "Date": "2022-04-21",
                "StartTime": "09:00",
                "EndTime": "09:30",
                "Timestamp": 1650524400,
                "AppointmentTypeId": 42,
                "ResourceId": 288,
            },
        ]
        expected_aggregations = [
            {
                "Date": "2022-04-21",
                "Times": [
                    {
                        "StartTime": "09:00",
                        "EndTime": "09:30",
                    },
                ],
            },
        ]
        self.assertEqual(
            expected_aggregations, aggregate_bookable_items_per_date(items)
        )
