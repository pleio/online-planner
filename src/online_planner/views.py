import logging
from datetime import datetime

import pytz
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .datetime_conversion import as_cet_datetime, as_utc_datetime
from .exceptions import BackendError
from .online_meetings import (
    aggregate_bookable_items_per_date,
    extract_items_from_xml,
    get_agenda_params,
    get_date_interval,
    get_or_create_customer_params,
    get_standard_create_appointment_params,
    online_meetings_get,
    online_meetings_put,
)
from .serializers import AppointmentSchema, AppointmentSerializer
from .video_call import get_video_call_params

logger = logging.getLogger(__name__)


class OnlineMeetingEndpoint(APIView):
    """Base class to query a meeting backend endpoint without parameters."""

    method: str
    resource: str

    # The Swagger UI shows the doc-string of this method. Because of that, the
    # doc-string is written for that context.
    def get(self, request, format=None):
        """Retrieve the elements of the type denoted by the endpoint name."""
        response = {"status": {"success": True, "message": ""}, "items": []}

        try:
            xml_content = online_meetings_get(self.method)
            response["items"] = extract_items_from_xml(self.resource, xml_content)
        except BackendError as e:
            response["status"]["success"] = False
            response["status"]["message"] = str(e)

        return Response(response)


class AppointmentType(OnlineMeetingEndpoint):
    method = "getAppointmentTypes"
    resource = "AppointmentType"


class Bookable(APIView):
    def get(self, request, appointment_type, format=None):
        """Retrieve the bookable times as known by the meeting backend.

        This endpoint retrieves the bookable times from this moment until the
        end of the next month for the appointment type that you provide in the
        URL.

        The bookable times are stored in a dictionary with key "items" whose
        value lists the bookable times per day.

        """

        response = {"status": {"success": True, "message": ""}, "items": []}

        try:
            xml_content = online_meetings_get("getAgendas")

            agendas = extract_items_from_xml("Agenda", xml_content)
            agenda_id = agendas[0]["Id"]

            # compute the date range starting now until (and including) the
            # last day of the month
            #
            # The next statement returns the date of "now", where "now" is the
            # time according to CET taking possible daylight saving time into
            # account. This is the timezone of the meeting backend.
            today = datetime.now(pytz.timezone("CET")).date()

            first_date, last_date = get_date_interval(today)

            endpoint_params = {
                "AgendaId": agenda_id,
                "AppointmentTypeId": int(appointment_type),
                "Date": first_date.strftime("%Y-%m-%d"),
                "EndDate": last_date.strftime("%Y-%m-%d"),
            }
            xml_content = online_meetings_get("getBookableTimes", endpoint_params)
            response["items"] = aggregate_bookable_items_per_date(
                self._extract_bookable_items(xml_content)
            )

        except BackendError as e:
            response["status"]["success"] = False
            response["status"]["message"] = str(e)

        return Response(response)

    @staticmethod
    def _extract_bookable_items(xml_content):
        """Extract the dictionary of bookable times from the XML response content.

        The given XML response contains a list of bookable items that each have
        a Date, StartTime and EndTime tag, all specified in CET. This Django
        app uses dates & times in UTC so this method converts their values
        accordingly.

        """
        bookable_items = extract_items_from_xml(
            "BookableTime", xml_content, sort_key="Timestamp"
        )
        for bookable_item in bookable_items:
            start_datetime = as_utc_datetime(
                bookable_item["Date"], bookable_item["StartTime"]
            )
            bookable_item["Date"] = start_datetime.strftime("%Y-%m-%d")
            bookable_item["StartTime"] = start_datetime.strftime("%H:%M")

            end_datetime = as_utc_datetime(
                bookable_item["Date"], bookable_item["EndTime"]
            )
            bookable_item["EndTime"] = end_datetime.strftime("%H:%M")

        return bookable_items


class Appointment(APIView):

    schema = AppointmentSchema()

    def post(self, request, format=None):
        """Create a new appointment at the meeting backend."""
        request_params = self._retrieve_validated_params(request)

        get_params_functions = [
            get_standard_create_appointment_params,
            get_agenda_params,
            get_or_create_customer_params,
        ]
        if request_params["reserve_room"]:
            get_params_functions.append(get_video_call_params)

        response = {"status": {"success": True, "message": ""}, "items": []}
        try:
            set_appointment_params = {}
            for get_params in get_params_functions:
                set_appointment_params |= get_params(request_params)

            xml_content = self._schedule_appointment(set_appointment_params)
            response["items"] = extract_items_from_xml("Appointment", xml_content)
            status_code = status.HTTP_201_CREATED
        except BackendError as e:
            response["status"]["success"] = False
            response["status"]["message"] = str(e)
            status_code = status.HTTP_200_OK

        return Response(response, status_code)

    def _retrieve_validated_params(self, request):
        # Deserialize the input parameters, that is, transform them to a dict
        # of native datatypes. This step validates the input parameters.
        serializer = AppointmentSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Serialize the native datatypes back to a "simple" representation,
        # which lets them have the right format. This is especially useful for
        # date and time fields as only the serialized version will zero-pad the
        # numbers, so month "05" instead of month "5" and hour "09" instead of
        # "9".
        return serializer.to_representation(serializer.data)

    @staticmethod
    def _schedule_appointment(endpoint_params):
        """Schedule the appointment specified by the given endpoint parameters.

        This method schedules an appointment using the setAppointment method of
        the meeting backend.

        The given endpoint parameters - a dictionary - contains a Date and
        StartTime key that are both specified in UTC. As the meeting backend
        expects them in CET and expects them to take daylight saving time into
        account, this method converts their values accordingly.

        """
        start_datetime = as_cet_datetime(
            endpoint_params["Date"], endpoint_params["StartTime"]
        )
        endpoint_params["Date"] = start_datetime.strftime("%Y-%m-%d")
        endpoint_params["StartTime"] = start_datetime.strftime("%H:%M")

        return online_meetings_put("setAppointment", endpoint_params)
