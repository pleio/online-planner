import unittest
import unittest.mock as mock

import pytest
import requests
import responses

from online_planner.exceptions import VideoCallBackendRequestError
from online_planner.settings_container import SettingsContainerBase
from online_planner.video_call import get_video_call_params


class TestsGetVideoCallParams(unittest.TestCase):
    def setUp(self):
        self.settings = SettingsContainerBase().build()

        self.request_params = {
            "date": "2022-06-09",
            "start_time": "16:00",
            "first_name": "Donald",
            "last_name": "Duck",
            "users": {"name_host": "Belastingdienst", "name_guest": "Donald Duck"},
        }

    @responses.activate()
    def test_reserve_room(self):
        # specify how requests.posts call should access the endpoint that
        # allocates a room and what the endpoint should return
        expected_payload = {
            "date": "2022-06-09",
            "language": "nl",
            "time_start": "16:00",
            "time_finish": "17:00",
            "users": {"name_host": "Employee", "name_guest": "Donald Duck"},
        }
        responses.post(
            # specify expected input
            self.settings.get_video_api_url(),
            match=[responses.matchers.json_params_matcher(expected_payload)],
            # specify response
            status=201,
            json={
                "status": {"success": True},
                "urls": {
                    "host": "https://example.com/host",
                    "guest": "https://example.com/guest",
                },
            },
        )

        endpoint_params = get_video_call_params(self.request_params)

        expected_endpoint_params = {
            "VideoCallHostURL": "https://example.com/host",
            "VideoCallGuestURL": "https://example.com/guest",
        }
        self.assertEqual(expected_endpoint_params, endpoint_params)

    @pytest.mark.usefixtures("only_log_critical")
    @responses.activate()
    def test_raise_correct_exception_when_status_code_indicates_error(self):
        responses.post(
            self.settings.get_video_api_url(),
            status=500,
            # note the trailing comma in the JSON response body: this payload
            # cannot be converted to JSON
            body='{"status": {"success": false, }}',
        )

        with self.assertRaises(VideoCallBackendRequestError) as cm:
            _ = get_video_call_params(self.request_params)

        self.assertEqual(
            "Request to reserve room at video call backend failed: unable to "
            "decode JSON payload of response (500)",
            str(cm.exception),
        )

    @responses.activate()
    def test_log_error_codes_when_response_contains_error_codes(self):
        responses.post(
            self.settings.get_video_api_url(),
            status=500,
            json={
                "status": {
                    "success": False,
                    "error_codes": ["error code #0", "error code #1"],
                },
            },
        )

        with mock.patch("online_planner.video_call.logger") as logger:
            with self.assertRaises(VideoCallBackendRequestError):
                _ = get_video_call_params(self.request_params)

        logger.error.assert_called_with(
            "Request to reserve room at video call backend failed: error code "
            "#0, error code #1 (500)"
        )

    @responses.activate()
    def test_log_error_codes_when_response_status_specifies_failure(self):
        responses.post(
            self.settings.get_video_api_url(),
            status=200,
            json={
                "status": {
                    "success": False,
                    "error_codes": ["error code #0", "error code #1"],
                },
            },
        )

        with mock.patch("online_planner.video_call.logger") as logger:
            with self.assertRaises(VideoCallBackendRequestError):
                _ = get_video_call_params(self.request_params)

        logger.error.assert_called_with(
            "Request to reserve room at video call backend failed: error code "
            "#0, error code #1 (200)"
        )

    @responses.activate()
    def test_log_response_reason_when_response_does_not_contain_error_codes(self):
        responses.post(
            self.settings.get_video_api_url(),
            status=400,
            json={
                "status": {
                    "success": False,
                },
            },
        )

        with mock.patch("online_planner.video_call.logger") as logger:
            with self.assertRaises(VideoCallBackendRequestError):
                _ = get_video_call_params(self.request_params)

        logger.error.assert_called_with(
            "Request to reserve room at video call backend failed: Bad Request (400)"
        )

    @pytest.mark.usefixtures("only_log_critical")
    @responses.activate()
    def test_raise_correct_exception_when_endpoint_cannot_be_reached(self):
        # raise exception when post is executed
        responses.post(
            self.settings.get_video_api_url(), body=requests.ConnectionError()
        )

        with self.assertRaises(VideoCallBackendRequestError) as cm:
            _ = get_video_call_params(self.request_params)

        self.assertEqual(
            "Request to reserve room at video call backend failed: POST raised "
            "requests.exceptions.ConnectionError",
            str(cm.exception),
        )
