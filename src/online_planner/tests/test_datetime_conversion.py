import unittest

from online_planner.datetime_conversion import as_cet_datetime, as_utc_datetime


class TestConvertCETToUTC(unittest.TestCase):
    def test_convert_while_daylight_savings_time_is_active(self):
        iso_date = "2022-06-13"
        iso_time = "14:30"

        dt = as_utc_datetime(iso_date, iso_time)
        self.assertEqual("2022-06-13 12:30 +0000", dt.strftime("%Y-%m-%d %H:%M %z"))

    def test_convert_while_daylight_savings_time_is_inactive(self):
        iso_date = "2022-02-13"
        iso_time = "14:30"

        dt = as_utc_datetime(iso_date, iso_time)
        self.assertEqual("2022-02-13 13:30 +0000", dt.strftime("%Y-%m-%d %H:%M %z"))

    def test_convert_to_next_day_while_daylight_savings_time_is_inactive(self):
        iso_date = "2022-02-13"
        iso_time = "00:59"

        dt = as_utc_datetime(iso_date, iso_time)
        self.assertEqual("2022-02-12 23:59 +0000", dt.strftime("%Y-%m-%d %H:%M %z"))


class TestConvertUTCToUTC(unittest.TestCase):
    def test_convert_while_daylight_savings_time_is_active(self):
        iso_date = "2022-06-13"
        iso_time = "14:30"

        dt = as_cet_datetime(iso_date, iso_time)
        self.assertEqual("2022-06-13 16:30 +0200", dt.strftime("%Y-%m-%d %H:%M %z"))

    def test_convert_while_daylight_savings_time_is_inactive(self):
        iso_date = "2022-02-13"
        iso_time = "14:30"

        dt = as_cet_datetime(iso_date, iso_time)
        self.assertEqual("2022-02-13 15:30 +0100", dt.strftime("%Y-%m-%d %H:%M %z"))

    def test_convert_to_next_day_while_daylight_savings_time_is_inactive(self):
        iso_date = "2022-02-12"
        iso_time = "23:00"

        dt = as_cet_datetime(iso_date, iso_time)
        self.assertEqual("2022-02-13 00:00 +0100", dt.strftime("%Y-%m-%d %H:%M %z"))
