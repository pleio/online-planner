class BackendError(Exception):
    """The call to the meeting backend failed."""


class BackendRequestError(BackendError):
    """The actual request to the meeting backend failed."""


class BackendResponseContentError(BackendError):
    """The content of the response by the meeting backend specified an error."""

    def __init__(self, reason: str, code: str):
        super().__init__(
            f"Meeting backend response content specifies a failure: {reason} ({code})"
        )


class MultipleEnabledCustomersError(BackendError):
    """There are multiple enabled customers that satisfy the search criteria."""

    def __init__(self):
        super().__init__(
            "Meeting backend response content specifies multiple customers that satisfy "
            "the search criteria"
        )


class CreateNewCustomerError(BackendError):
    """The request to create a new customer did not yield an enabled customer.

    The response (to the request to create a new customer) should specify a
    new, enabled customer. This exception indicates that the response did not
    specify such a customer.

    """

    def __init__(self):
        super().__init__(
            "Meeting backend response content does not specify a new enabled customer"
        )


class VideoCallBackendRequestError(BackendError):
    """The actual request to the video call backend failed."""
